
#include <Adafruit_NeoPixel.h>

#define BUTTON_PIN_DX   2    // Digital IO pin connected to the button.  This will be
#define BUTTON_PIN_SX   3 
                          // driven with a pull-up resistor so the switch should
                          // pull the pin to ground momentarily.  On a high -> low
                          // transition the button press logic will execute.

#define PIXEL_PIN    6    // Digital IO pin connected to the NeoPixels.

#define NPIX 8
#define noiseGAP 240

// Parameter 1 = number of pixels in strip,  neopixel stick has 8
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick
Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(NPIX, 6, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(NPIX, 7, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip3 = Adafruit_NeoPixel(NPIX, 8, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip4 = Adafruit_NeoPixel(NPIX, 9, NEO_GRB + NEO_KHZ800);

bool oldState = HIGH;
int showType = 0;

uint8_t tipoAvanti = 1;
uint8_t tipoDietro = 1;
uint8_t frecciaDx = 0;
uint8_t frecciaSx = 0;
uint8_t cambioMod = 0;

uint8_t interruptDX = 0;
uint8_t interruptSX = 0;

uint32_t lastDX = 0;
uint32_t lastSX = 0;

uint32_t giallo;
uint32_t bianco;
uint32_t rosso;
uint32_t spento;

void setTastoDx(){
  interruptDX = 1;
}

void setTastoSx(){
  interruptSX = 1;
  
}

void setup() {

    Serial.begin(9600);
  
  pinMode(BUTTON_PIN_DX, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN_DX), setTastoDx, RISING);
  
  pinMode(BUTTON_PIN_SX, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN_SX), setTastoSx, RISING);
  
  
  strip1.begin();
  strip1.show(); // Initialize all pixels to 'off'
  strip2.begin();
  strip2.show(); // Initialize all pixels to 'off'
  strip3.begin();
  strip3.show(); // Initialize all pixels to 'off'
  strip4.begin();
  strip4.show(); // Initialize all pixels to 'off'

  //giallo = strip1.Color(255, 160, 0);
  giallo = strip1.Color(60, 30, 0);
  bianco = strip1.Color(127, 127, 127);
  rosso = strip1.Color(255, 0, 0);
  spento = strip1.Color(0, 0, 0);
  
}

/*      disposizione luci
 *       
 *      1 ******** 2
 *            *
 *            *
 *            *
 *      3 ******** 4
 */


void loop() {
/*  
Serial.print("tipo avanti ");
Serial.println(tipoAvanti);
Serial.print("tipo dietro ");
Serial.println(tipoDietro);
Serial.print("freccia DX ");
Serial.println(frecciaDx);
Serial.print("freccia SX ");
Serial.println(frecciaSx);
Serial.println(" ");
Serial.println(" ");
Serial.println(" ");
*/

  if(interruptDX){
    Serial.println("interruptDX");
    if( lastDX == 0 ){
      lastDX = millis();
      if(!digitalRead(BUTTON_PIN_SX)){
        tipoAvanti++;
        cambioMod = 1;
      }else{
        if(cambioMod){
          cambioMod = 0;
        }
        else{
          frecciaDx = !frecciaDx;
        }
      }
      Serial.println("interruptDX - eseguito");
    }else if(millis() - lastDX >= noiseGAP){
      lastDX = 0;
      
    }else{
      lastDX = millis();
    }
    interruptDX = 0;

  }
  
  if(interruptSX){
    Serial.println("interruptSX");
    if( lastSX == 0 ){
      lastSX = millis();
      if(!digitalRead(BUTTON_PIN_DX)){
        tipoDietro++;
        cambioMod = 1;
      }else{
        if(cambioMod){
          cambioMod = 0;
        }else{
          frecciaSx = !frecciaSx;
        }
      }
      Serial.println("interruptSX - eseguito");
    }else if(millis() - lastSX >= noiseGAP){
      lastSX = 0;
    }else{
      lastSX = millis();
    }

   interruptSX = 0;

  }

  switch(tipoAvanti){
    case 1: if(frecciaDx){
                theaterChaseMio(bianco,1);
                frecciaAvanti(2);
            }
            if(frecciaSx){
                theaterChaseMio(bianco,2);
                frecciaAvanti(1);
            }
            if(!frecciaSx && !frecciaDx){
                theaterChaseMio(bianco,1);
                theaterChaseMio(bianco,2);
            }
            if(frecciaSx && frecciaDx){
                theaterChaseMio(bianco,1);
                theaterChaseMio(bianco,2);
                frecciaSx = 0;
                frecciaDx = 0;
            }
            break;
     case 2: //----- come sopra ------
             break;
  }

  switch(tipoDietro){
    case 1: if(frecciaDx){
                theaterChaseMio(rosso,3);
                frecciaDietro(4);
            }
            if(frecciaSx){
                theaterChaseMio(rosso,4);
                frecciaDietro(3);
            }
            if(!frecciaSx && !frecciaDx){
                theaterChaseMio(rosso,3);
                theaterChaseMio(rosso,4);
            }
             if(frecciaSx && frecciaDx){
                theaterChaseMio(rosso,3);
                theaterChaseMio(rosso,4);
                frecciaSx = 0;
                frecciaDx = 0;
            }
            break;
     case 2: //----- come sopra ------
             break;
  }

  //theaterChaseMio(bianco,1);
  //theaterChaseMio(bianco,2);
  //theaterChaseMio(rosso,3);
  //theaterChaseMio(rosso,4);
  
  //frecciaAvanti(1);
  //frecciaAvanti(2);
  //frecciaDietro(3);
  //frecciaDietro(4);
  delay(40);
   // White
  
  
}



void frecciaDietro(uint8_t nStrip){
  
  static uint8_t passo = 0;
 
  if(passo < NPIX) {
    
    switch(nStrip){
        case 3: strip3.setPixelColor(passo, giallo);
                strip3.show();
                break; 
        case 4: strip4.setPixelColor(passo, giallo);
                 strip4.show();
                break;
    }
  }
  if(passo >= NPIX) {
     
    switch(nStrip){
        case 3: strip3.setPixelColor(passo - NPIX, spento);
                strip3.show();
                break;
        case 4: strip4.setPixelColor(passo - NPIX, spento);
                strip4.show();
                break;
    }
  }
  
  if(passo < 15){
    passo++;
  }else{
    passo = 0;
  }
}



void frecciaAvanti(uint8_t nStrip){
  
  static uint8_t passo = 15;
 
  if(passo >= NPIX) {
    
    switch(nStrip){
        case 1: strip1.setPixelColor(passo - NPIX, giallo);
                strip1.show();
                break;
        case 2: strip2.setPixelColor(passo - NPIX, giallo);
                strip2.show();
                break;
    }
  }
  if(passo < NPIX) {
     
    switch(nStrip){
        case 1: strip1.setPixelColor(passo, spento);
                strip1.show();
                break;
        case 2: strip2.setPixelColor(passo, spento);
                strip2.show();
                break;
    }
  }
  
  if(passo > 0){
    passo--;
  }else{
    passo = 15;
  }
}



//Theatre-style crawling lights.
void theaterChaseMio(uint32_t c, uint8_t nStrip) {
  
  static int16_t passo = 0;
  static uint8_t lastStrip = 4;

  for (int i=0; i < NPIX; i=i+3) {
        switch(nStrip){
           case 1: strip1.setPixelColor(i+passo-1, 0);
                   strip1.show();
                   break;
           case 2: strip2.setPixelColor(i+passo-1, 0);
                   strip2.show();
                   break;
           case 3: strip3.setPixelColor(i+passo-1, 0);
                   strip3.show();
                   break;
           case 4: strip4.setPixelColor(i+passo-1, 0);
                   strip4.show();
                   break;
       }
   }
   
   
   for (int i=0; i < NPIX; i=i+3) {
        switch(nStrip){
           case 1: strip1.setPixelColor(i+passo, c);
                   strip1.show();
                   break;
           case 2: strip2.setPixelColor(i+passo, c);
                   strip2.show();
                   break;
           case 3: strip3.setPixelColor(i+passo, c);
                   strip3.show();
                   break;
           case 4: strip4.setPixelColor(i+passo, c);
                   strip4.show();
                   break;
       }
        
    }
    Serial.println(lastStrip);
    if (nStrip <= lastStrip){
      
      if(passo < 3)
        passo++;
      else
        passo = 0;  
   }
    lastStrip = nStrip;
  
}
